package exception;

public class exception extends Exception{
	//VARIABLE PARA RECONOCER EL CODIGO DEL ERROR
	private int codigo;
	
	//CONTRUCTOR DE CLASE
	public exception(int codigo) {
		super();
		this.codigo=codigo;
	}
	
	//METODO PERSONALIZADO GET MESSAGE
	@Override
	public String getMessage(){
		String message="";
		
		switch (codigo) {
		case 1:
			message=("Excepcion capturada con mensaje: Esto es un objeto Exception");
			break;
	}
		return message;
	}
}
