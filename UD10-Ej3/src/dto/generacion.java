package dto;
import java.util.Random;

import exception.exception;

public class generacion {
	
	//INSTANCIA DE LA RANDOM CLASS
	Random aleatorio = new Random();
	
	//ATRIBUTOS
	private int numero;
	private boolean par;
	
	//CONSTRUCTOR
	public generacion(){
		this.numero=aleatorio.nextInt(1000);
		this.par=false;
	}
	
	//GETTERS Y SETTERS
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isPar() {
		return par;
	}

	public void setPar(boolean par) {
		this.par = par;
	}
	
	//METODO
	public void ejecucion() {
		System.out.println("Generando n�mero aleatorio...");
		System.out.println("El n�mero aleatorio generado �s: "+getNumero());
		try {
			if ((getNumero()%2)==0) {
				throw new exception(1);
			}else {
				throw new exception(2);
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
	}
}
