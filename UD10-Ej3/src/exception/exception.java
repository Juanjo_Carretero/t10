package exception;

public class exception extends Exception{
	//VARIABLE PARA RECONOCER EL CODIGO DEL ERROR
	private int codigo;
	
	//CONTRUCTOR DE CLASE
	public exception(int codigo) {
		super();
		this.codigo=codigo;
	}
	
	//METODO PERSONALIZADO GET MESSAGE
	@Override
	public String getMessage(){
		String message="";
		
		switch (codigo) {
		case 1:
			message=("Es par ");
			break;
		case 2:
			message=("Es impar");
			break;
		}
		return message;
	}
}
