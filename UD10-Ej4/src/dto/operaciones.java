package dto;

import exception.exception;

public class operaciones {
	
	//ATRIBUTOS
	double n1;
	double n2;
	
	//CONSTRUCTOR DEFAULT
	public operaciones(){
		this.n1=0;
		this.n2=0;
	}
	
	//CONSTRUCTOR
	public operaciones(double n1, double n2){
		this.n1=n1;
		this.n2=n2;
	}
	
	//CONSTRUCTOR RAIZES
	public operaciones(double n1){
		this.n1=n1;
	}
	
	//GETTERS Y SETTERS
	public double getN1() {
		return n1;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public double getN2() {
		return n2;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}
	
	//METODO PARA LA SUMA
	public double suma() {
		double resultado=0;
		
		try {
			if (getN2() < 0 || getN1() < 0) {
				throw new exception(4);
			}else {
				resultado = getN1() + getN2();
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO PARA LA RESTA
	public double resta() {
		double resultado=0;
		
		try {
			if (getN2() < 0 || getN1() < 0) {
				throw new exception(4);
			}else {
				resultado = getN1() - getN2();
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO PARA LA MULTIPLICACION
	public double multiplicacion() {
		double resultado=0;
		
		try {
			if (getN2() < 0 || getN1() < 0) {
				throw new exception(4);
			}else {
				resultado = getN1() * getN2();
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO PARA LA DIVISI�N
	public double division() {
		double resultado=0;

		try {
			if (getN2() == 0) {
				throw new exception(1);
			}else {
				resultado = getN1() / getN2();
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO PARA LA POTENCIA
	public double potencia() {
		double resultado=0;
		
		try {
			if (getN2() < 0 || getN1() < 0) {
				throw new exception(4);
			}else {
				resultado = Math.pow(getN1(), getN2());
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO RAIZ CUADRADA
	public double raizcuadrada() {
		double resultado=0;

		try {
			if (getN1() < 0) {
				throw new exception(2);
			}else {
				resultado = Math.sqrt(n1);
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}
	
	//METODO RAIZ CUBICA
	public double raizcubica() {
		double resultado=0;

		try {
			if (getN1() < 0) {
				throw new exception(3);
			}else {
				resultado = Math.cbrt(n1);
			}
		} catch (exception e) {
			System.out.println(e.getMessage());
		}
		
		return resultado;
	}

	
	
}
