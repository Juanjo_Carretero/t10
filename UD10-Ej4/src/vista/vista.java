package vista;
import dto.operaciones;
import java.util.InputMismatchException;
import java.util.Scanner;

public class vista {
	//AQUI VA EL SWITCH PARA ELEGIR QUE OPERACION SE HACE
	public static void vistamenu(){
		
		String opcion = "";
		Scanner scan = new Scanner(System.in);
		double resultado=0;
		double n1,n2;
		
		//MUESTRO POR PANTALLA QUE OPERACIONES SE PUEDEN HACER
        System.out.println("suma");
        System.out.println("resta");
        System.out.println("multiplicacion");
        System.out.println("divisi�n");
        System.out.println("pot�ncia");
        System.out.println("r.cuadrada");
        System.out.println("r.cubica");
            
        	//REALIZO TRY-CATCH
            try {
            	
            	//SELECCION DE LA OPERACI�N
                System.out.println("Escribe una de las operaciones");
                opcion = scan.next();
                
                //SWITCH CON LAS DIFERENTES OPERACIONES
                switch (opcion) {
                
			        case "suma":
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	System.out.println("N2");
			        	n2 = scan.nextDouble();
			        	
			        	operaciones o1 = new operaciones(n1,n2);
			        	
			        	resultado = o1.suma();
			        	
			        	System.out.println("El resultado de la suma �s: "+resultado);
			        	
			        	break;
			        	
			        case "resta":
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	System.out.println("N2");
			        	n2 = scan.nextDouble();
			        	
			        	operaciones o2 = new operaciones(n1,n2);
			        	
			        	resultado = o2.resta();
			        	
			        	System.out.println("El resultado de la resta �s: "+resultado);
			        	
			        	break;
			        case "multiplicacion":
			        	
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	System.out.println("N2");
			        	n2 = scan.nextDouble();
			        	
			        	operaciones o3 = new operaciones(n1,n2);
			        	
			        	resultado = o3.multiplicacion();
			        	
			        	System.out.println("El resultado de la multiplicacion �s: "+resultado);
			        	
			        	break;
			            
			        case "division":
			            
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	System.out.println("N2");
			        	n2 = scan.nextDouble();
			        	
			        	operaciones o4 = new operaciones(n1,n2);
			        	
			        	resultado = o4.division();
			        	
			        	System.out.println("El resultado de la divsi�n �s: "+resultado);
			        	
			            break;
			            
			        case "pot�ncia":
			        	
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	System.out.println("N2");
			        	n2 = scan.nextDouble();
			        	
			        	operaciones o5 = new operaciones(n1,n2);
			        	
			        	resultado = o5.potencia();
			        	
			        	System.out.println("El resultado de la pot�ncia �s: "+resultado);
			        	
			        	break;
			        	
			        case "r.cuadrada":
			        	
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	operaciones o6 = new operaciones(n1);
			        	
			        	resultado = o6.raizcuadrada();
			        	
			        	System.out.println("El resultado de la raiz cuadrada �s: "+resultado);
			        	
			        	break;
			        case "r.cubica":
			        	
			        	System.out.println("N1");
			        	n1 = scan.nextDouble();
			        	
			        	operaciones o7 = new operaciones(n1);
			        	
			        	resultado = o7.raizcubica();
			        	
			        	System.out.println("El resultado de la raiz cubica �s: "+resultado);
			        	
			        	break;
			    }
	    } catch (InputMismatchException e) {
	        System.out.println("Debes insertar el nombre de alguna operacion v�lida");
	        }
	    }
	
		}

