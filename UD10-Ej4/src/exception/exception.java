package exception;

public class exception extends Exception{
	//VARIABLE PARA RECONOCER EL CODIGO DEL ERROR
	private int codigo;
	
	//CONTRUCTOR DE CLASE
	public exception(int codigo) {
		super();
		this.codigo=codigo;
	}
	
	//METODO PERSONALIZADO GET MESSAGE
	@Override
	public String getMessage(){
		String message="";
		
		switch (codigo) {
		case 1:
			message=("No se puede poner un 0 en una divisi�n.");
			break;
		case 2:
			message=("Una raiz cuadrada no permite n�meros negativos");
			break;
		case 3:
			message=("Una raiz cubica no permite n�meros negativos");
			break;
		case 4:
			message=("No permito la introducci�n de n�mero negativos");
			break;
		}
		return message;
	}
}