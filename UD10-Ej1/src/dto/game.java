package dto;

import java.util.InputMismatchException;
import vista.scan;

public class game {
	
	//ATRIBUTOS
	private int numero;
	private boolean win;
	private int contador;
	
	//CONSTRUCTOR
	public game(){
		this.numero=(int) (Math.random()*5+1);
		this.contador=0;
		this.win=false;
	}

	
	//GETTERS Y SETTERS
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public boolean isWin() {
		return win;
	}

	public void setWin(boolean win) {
		this.win = win;
	}

	public int getContador() {
		return contador;
	}

	public void setContador(int contador) {
		this.contador = contador;
	}
	
	//METODOS
	public void jugar() {
		try {
			scan p = new scan();
			int intento=p.intento();
			if (intento==this.numero) {
				this.win=true;
			}else if (intento<this.numero) {
				System.out.println("El n�mero introducido es mas peque�o");
			}else if (intento>this.numero) {
				System.out.println("El n�mero introducido es mas grande");
			}
		} catch (InputMismatchException e) {
			System.out.println("No has introducido un valor valido");
		}finally {
			this.contador++;
		}
		if(this.win) 
			System.out.println("Has encontrado el n�mero en "+this.contador+" intentos.");
		
	}
	
	
	
	
}
